from os import listdir
from os.path import isfile, join
import csv
import sys

from report_section_helper import *
from summary_builder import normalize_summary

MEETING_SUMMARY = 'Meeting Summary'
FULL_NAME =  'Full Name'
PARTICIPANTS  = '2. Participants'
ACTIVITIES = '3. In-Meeting activities'

def file_meet_conditions(path, pattern, file_path):
    return isfile(join(path, file_path)) and file_path.endswith(pattern)


# discovery de "data" files
def discover_data_files(path='../data', pattern='csv'):
    return [join(path, f) for f in listdir(path) if file_meet_conditions(path, pattern, f)]


def get_file_data(path):
    with open(path, encoding='UTF-16') as reader:
        return list(csv.reader(reader, delimiter='\t'))


def get_summary_as_dict(summary):
    # convert list that contains "summary" lines into dict 
    summary_dict = {}
    for element in summary:
        _, row = element
        # TODO must guarante index 0 and 1 exist
        summary_dict[row[0]] = row[1]
    return summary_dict


def get_data_as_dict(data):
    if data == []:
        return data

    result = []
    _, header = data[0] # TODO must guarante index 0 exists
    for _, row in data:
        result.append(dict(zip(header,row)))
    return result



def normalize_raw_data(data):
    is_old_file_version = [MEETING_SUMMARY] in data

    # extract data sections
    summary =  extract_section_rows(data, MEETING_SUMMARY, FULL_NAME) if is_old_file_version else extract_section_rows(data, '1. Summary', PARTICIPANTS)
    participants  = [] if is_old_file_version else extract_section_rows(data, PARTICIPANTS, ACTIVITIES)
    in_meetings = extract_section_rows(data, FULL_NAME, include_start=True) if is_old_file_version else extract_section_rows(data, ACTIVITIES)
    
    # convert list that contains "summary" lines into dict 
    summary = get_summary_as_dict(summary)
    participants = get_data_as_dict(participants)
    in_meetings = get_data_as_dict(in_meetings)

    # normalize take "key": "value" and put into standard format "key": "value" 
    summary =  normalize_summary(summary)


def load_data():
    file_paths = discover_data_files() or []
    for fp in file_paths:
        # if not (
        #     'Python Training [week 6] - [week 13] - Attendance report 11-22-22.csv' in fp 
        #     # or 
        #     # 'meetingAttendanceReport(General) (8)' in fp
        #     ):
        #     continue
        print(f"processing {fp}")
        data = get_file_data(fp)
        normalize_raw_data(data)
            
            

load_data()